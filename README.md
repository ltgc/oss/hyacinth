# Hyacinth
💨 Build web projects with speed.

Documentation available at [kb.ltgc.cc](https://kb.ltgc.cc/hyacinth/).

## Key features
* No BS configurations, just build.
* Uses `esbuild` and `lightningcss` under the hood.
* Runtime agnostic, no support towards any single runtime.
* Directory structured.
